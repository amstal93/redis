## Talkative
- persistence: use both `RDB` and `AOF` [https://redis.io/topics/persistence](https://redis.io/topics/persistence)
- access control: use `ACL` [https://redis.io/topics/acl](https://redis.io/topics/acl)
- cache replacement policy: use `LRU` - Least Recently Used [https://redis.io/topics/lru-cache](https://redis.io/topics/lru-cache)

## Facts that you do not wanna know
- Redis means `RE`mote `DI`ctionary `S`erver

## Something noteworthy
#### 1. Keep the redis.conf up-to-date
You can look for latest config here: https://redis.io/topics/config. We keep a local copy for easy copypasta.
```bash
wget https://raw.githubusercontent.com/redis/redis/6.0/redis.conf -O redis.6.orig.conf
```

#### 2. Kick-in
All commands below will work, pass the password if you want:
```bash
redis-cli
redis-cli --pass default
docker run --rm -it --net container:rediz redis:6.2 redis-cli
docker run --rm -it --net host redis:6.2 redis-cli
```
